# Tensor Flow Testing

Tensorflow is installed at:

```
~/ tensorflow
```

To activate type:

```
$ source ~/tensorflow/bin/activate.csh
```

Promt should change to:
```
[tensorflow]$
```

Exit by typing comand:

```
(tensorflow)$ deactivate
```


## Amazon Server Login

go to:

```
http://console.aws.amazon.com
```

Login:

* neuralhfg@gmail.com
* perceptron

On Website Go to -> EC2 -> select instance -> connect. Copy/Paste the string (like this) to console:

```
ssh -i "HfG_pureTensor.pem" ubuntu@ec2-34-253-70-88.eu-west-1.compute.amazonaws.com
```